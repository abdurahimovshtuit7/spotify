//
//  ViewController.swift
//  Spotify
//
//  Created by Shaxboz Abdurahimov on 10/03/21.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Home"
        view.backgroundColor = .systemBackground
    }

}

